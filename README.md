# personal-awesomes

Just a collection of tools/libs for me to look at if I forgot the name.
Currently all of them are just for TS/JS projects.

## Framework agnostic (JS/TS specific)

- [👽 ufo](https://www.npmjs.com/package/ufo)
- [ESLint](https://www.npmjs.com/package/eslint)
- [Prettier](https://www.npmjs.com/package/prettier)
- [Tailwind CSS](https://tailwindcss.com/)
  - [@tailwindcss/typography](https://tailwindcss.com/docs/typography-plugin)
  - [@tailwindcss/forms](https://github.com/tailwindlabs/tailwindcss-forms)
  - [@tailwindcss/aspect-ratio](https://github.com/tailwindlabs/tailwindcss-aspect-ratio)
  - [@tailwindcss/container-queries](https://github.com/tailwindlabs/tailwindcss-container-queries)
- [eslint-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base)
- [eslint-airbnb-typescript](https://www.npmjs.com/package/eslint-config-airbnb-typescript)
- [eslint-config-prettier](https://www.npmjs.com/package/eslint-config-prettier)
- [eslint-plugin-prettier](https://www.npmjs.com/package/eslint-plugin-prettier)
- [eslint-plugin-tailwindcss](https://www.npmjs.com/package/eslint-plugin-tailwindcss)
- [eslint-plugin-deprecated](https://www.npmjs.com/package/eslint-plugin-deprecated)
- [@rushstack/eslint-patch](https://www.npmjs.com/package/@rushstack/eslint-patch)
- [prettier-plugin-tailwindcss](https://www.npmjs.com/package/prettier-plugin-tailwindcss) (don't need it if `eslint-plugin-tailwindcss` is installed and if `eslint-plugin-prettier` is also installed and configured it could be result into weird behavior)
- [fontsource](https://fontsource.org/)
- [tabler-icons](https://tabler.io/icons)
- [vime](https://vimejs.com/)
- [prisma](https://www.prisma.io/)
- [lucia-auth](https://lucia-auth.com/)
- [Zod](https://zod.dev/)
- [Tauri](https://tauri.app/)
- [Vite](https://vitejs.dev/)
- [Vitest](https://vitest.dev/)
- [Playwright](https://playwright.dev/)
- [Storybook](https://storybook.js.org/)
- [iconify](https://iconify.design/)
- [daisyUI](https://daisyui.com/)
- [Houdini](https://houdinigraphql.com/)
- [neodrag](https://www.neodrag.dev/)
- [ky](https://github.com/sindresorhus/ky)

## Svelte/SvelteKit

- [Skeleton](https://www.skeleton.dev/)
- [svelte-fluent](https://fluent-svelte.vercel.app/)
- [prettier-plugin-svelte](https://www.npmjs.com/package/prettier-plugin-svelte)
- [eslint-plugin-svelte](https://sveltejs.github.io/eslint-plugin-svelte/)
- [Superforms](https://superforms.vercel.app/)
- [Svelte Legos](https://sveltelegos.com/)
- [Layer Cake](https://layercake.graphics/)
- [svelte-spinkit](https://heithemmoumni.github.io/svelte-spinkit/)
- [Svelte Query](https://sveltequery.vercel.app/)
- [svelte-switch-case](https://github.com/l-portet/svelte-switch-case) (not sure if I should use this)
- [svelte-time](https://metonym.github.io/svelte-time/)
- [s-offline](https://github.com/vinayakkulkarni/s-offline)
- [svelte-sitemap](https://github.com/bartholomej/svelte-sitemap) Designed for SvelteKit `adapter-static` with prerender option (SSG)
- [Svelte-Headroom](https://github.com/collardeau/svelte-headroom)
- [svelte-portal](https://github.com/romkor/svelte-portal)
- [Svelte Meta Tags](https://github.com/oekazuma/svelte-meta-tags)
- [svelvg](https://github.com/metonym/svelvg)
- [svelte-lazy-loader](https://svelte-lazy-loader.sawyer.codes/)
- [SVELTE DND ACTION](https://github.com/isaacHagoel/svelte-dnd-action)
- [sveltekit-search-params](https://github.com/paoloricciuti/sveltekit-search-params)
- [svelte-put](https://svelte-put.vnphanquang.com/)

## Vue

- [Vue-Use](https://vueuse.org)
- [fluent-vue](https://github.com/fluent-vue/fluent-vue)
- [eslint-plugin-vue](https://github.com/vuejs/eslint-plugin-vue/releases)

## VSCode/VSCodium Plugins

I differentiate the extension between `+`(=very recommended), `nothing`(=matter of taste) and `*`(=optional mostly because it is framework specific)

- [auto rename tag](https://gitlab.com/redwan-hossain/auto-rename-tag-clone) + (Note: if you don't write HTML you don't need this)
- [better comments](https://github.com/aaron-bond/better-comments.git) +
- [bookmarks](https://github.com/alefragnani/vscode-bookmarks.git)
- [code spell checker](https://github.com/streetsidesoftware/vscode-spell-checker.git) +
- [EditorConfig for VS Code](https://github.com/editorconfig/editorconfig-vscode.git)
- [Error Lens](https://github.com/usernamehw/vscode-error-lens.git) + (could cause performance issues in huge projects)
- [Even Better TOML](https://github.com/tamasfe/taplo.git)
- [Git Graph](https://github.com/mhutchie/vscode-git-graph.git) (used rarely but looks neat, maybe I will use it more in future)
- [GitLens - Git Supercharged](https://github.com/gitkraken/vscode-gitlens.git) +
- [i18n Ally](https://github.com/lokalise/i18n-ally.git) +
- [Image preview](https://github.com/kisstkondoros/gutter-preview.git) +
- [Import Cost](https://github.com/wix/import-cost.git) + (JS and TS only)
- [intend-rainbow](https://github.com/oderwat/vscode-indent-rainbow.git) +
- [markdownlint](https://github.com/DavidAnson/vscode-markdownlint.git) +
- [Material Icon Theme](https://github.com/PKief/vscode-material-icon-theme.git) +
- [Native SVG Preview](https://github.com/SNDST00M/vscode-native-svg-preview.git) +
- [Output Colorizer](https://github.com/IBM-Bluemix/vscode-log-output-colorizer.git) +
- [PostCSS Language Support](https://github.com/csstools/postcss-language.git) (because of Tailwind I use PostCSS more often)
- [Project Manager](https://github.com/alefragnani/vscode-project-manager.git) +
- [region-folder](https://github.com/maptz/Maptz.VSCode.Extensions.customfolding)
- [ShellCheck](https://github.com/vscode-shellcheck/vscode-shellcheck.git) *
- [SQLTools](https://github.com/mtxr/vscode-sqltools) (if you work with databases)
- [Svelte for VS Code](https://github.com/sveltejs/language-tools.git) *
- [Tailwind CSS IntelliSense](https://github.com/tailwindlabs/tailwindcss-intellisense.git) *
- [Tauri](https://github.com/tauri-apps/tauri-vscode.git) *
- [Todo Tree](https://github.com/Gruntfuggly/todo-tree.git) + (if `better-comments` is installed I would recommend to disable `highlighting` of this extension)
- [Total Typescript](https://github.com/mattpocock/ts-error-translator.git)
- [YAML](https://github.com/redhat-developer/vscode-yaml.git)
- [Prettier - Code formatter](https://github.com/prettier/prettier-vscode.git) (don't need it if `eslint-plugin-prettier` is installed)

## Some libraries/tools which sounds interesting but not sure what to make of them yet

- [hotscript](https://github.com/gvergnaud/hotscript)
- [@changeset/cli](https://www.npmjs.com/package/@changesets/cli)
- [@total-typescript/ts-reset](https://github.com/total-typescript/ts-reset)
